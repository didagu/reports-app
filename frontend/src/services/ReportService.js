import axios from 'axios';

const url = 'http://localhost:4000/api/reports/';

class ReportService {
    // get all reports
    static async getReports() {
        try {
            const response = await axios.get(url);
            const { data } = response;
            return data.map((report) => ({
                ...report,
                createdAt: (new Date(report.createdAt)).toLocaleDateString(),
            }));
        } catch (e) {
            throw new Error(e);
        }
    }

    // insert report
    static insertReport(report) {
        return axios.post(url, {
            ...report,
        });
    }

    // delete report
    static deleteReport(id) {
        return axios.delete(`${url}${id}`);
    }
}

export default ReportService;
