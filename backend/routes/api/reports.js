const express = require('express');
const mongodb = require('mongodb');
const dbClient = require('../../DB/dbConnector');

const router = express.Router();
const DB_NAME = 'fullstackapp';

//Get reports
router.get('/', async (req, res) => {
    const reports = await loadReportsCollection();
    res.send(await reports.find({}).toArray());
});

//Add report
router.post('/', async (req, res) => {
    const reports = await loadReportsCollection();
    await reports.insertOne({
        teamMemberName: req.body.teamMemberName,
        calendarWeek: req.body.calendarWeek,
        currentIssues: req.body.currentIssues,
        upcomingIssues: req.body.upcomingIssues,
        createdAt: new Date()
    }).catch(e => console.log(e));
    res.status(201).send();
});

//Delete report
router.delete('/:id', async (req, res) => {
    const reports = await loadReportsCollection();
    await reports.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });
    res.status(200).send();
});

async function loadReportsCollection() {
    return dbClient.db(DB_NAME).collection('reports');
}

module.exports = router;
