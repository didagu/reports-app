const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

//middleware
app.use(bodyParser.json());
app.use(cors());

const reports = require('./routes/api/reports');
app.use('/api/reports', reports);

const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`Server running on port: ${port}`));
