
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config({ path: 'backend/.env' });
}

const MongoClient = require('mongodb').MongoClient;
const uri = process.env.MONGODB_URI;
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();

module.exports = client;
